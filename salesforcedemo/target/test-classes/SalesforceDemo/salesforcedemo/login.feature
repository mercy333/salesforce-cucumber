Feature: Login to salesforce

  Scenario: Salesforce login
    Given sales force credentials
    When verify login
    Then verify loginsuccessorfailure
    
  Scenario: Salesforce login
    Given searchforwritingteststring
    When search
    Then verifyswritingtestpageislisted
  Scenario:
  	Given verify search string page is loaded
  	When click on testing apex link
  	Then verify apex link page is loaded or not
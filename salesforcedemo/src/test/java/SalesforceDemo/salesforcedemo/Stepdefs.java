package com.salesforce.salesforce;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Stepdefs {

	WebElement apexLinkWebElement = null;

	static {
		System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
	}
	private final WebDriver driver = new ChromeDriver();

	// scenario1:login
	@Given("^sales force credentials$")
	public void sales_force_credentials() throws Exception {
		driver.get("https://developer.salesforce.com/");
		driver.findElement(By.id("login-button")).click();
	}

	@When("^verify login$")
	public void verify_login() throws Exception {

		driver.findElement(By.id("username")).sendKeys("almercy333@gmail.com");
		driver.findElement(By.id("password")).sendKeys("Test123!");
		driver.findElement(By.id("Login")).click();

	}

	@Then("^verify loginsuccessorfailure$")
	public void verify_loginsuccessorfailure() throws Exception {

		new WebDriverWait(driver, 50).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {

				// return driver.getTitle().startsWith("Home");
				return true;
			}
		});

	}

	// scenario2: search string "Writing Tests"
	@Given("^searchforwritingteststring$")
	public void searchforwritingteststring() throws Exception {
		driver.get("https://developer.salesforce.com/");
		WebElement searchWebElement = driver.findElement(By.id("st-search-input"));
		searchWebElement.clear();
		searchWebElement.sendKeys("Writing Tests");
		searchWebElement.sendKeys(Keys.ENTER);
		
	}

	@When("^search$")
	public void search() throws Exception {
		
		List<WebElement> list = driver.findElements(By.className("result__title"));
		for (WebElement webElement : list) {
			if (webElement.getText().startsWith("Writing Tests")) {
				webElement.click();
				break;
			}

		}
		
		
	}

	@Then("^verifyswritingtestpageislisted$")
	public void verifyswritingtestpageislisted() throws Exception {
		
		Thread.sleep(5000);
		Assert.assertTrue("Writing Tests", driver.getTitle().equals("Writing Tests"));	
		
	}
	
	@Given("^verify search string page is loaded$")
	public void verify_search_string_page_is_loaded() throws Exception {

		apexLinkWebElement = driver.findElement(By.linkText("Testing Apex"));
	}

	@When("^click on testing apex link$")
	public void click_on_testing_apex_link() throws Exception {
		apexLinkWebElement.click();
		Thread.sleep(5000);
	}

	@Then("^verify apex link page is loaded or not$")
	public void verify_apex_link_page_is_loaded_or_not() throws Exception {
		
		String verifyString = "Understanding Testing in Apex";
	 
		WebElement  webElement  = driver.findElement(By.linkText("Understanding Testing in Apex"));
		
		Assert.assertEquals(verifyString, webElement.getText());
	}

	@After
	public void closeBrowser()
	{
		driver.findElement(By.id("user-info-logout")).click();
		driver.quit();
	}
}